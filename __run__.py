from game import GameMapState,RemainingPiecesState,TurnState,GameMapGUI,GameGUI,PieceListGUI,bruteAI, playTurnGUI,notify,GameController,ScorePaneGUI
from java.lang import Thread, Math
'''
A location for the main method and test methods of the game. This file is invoked whether you run JNLP or any other method except for jython main.py (which subsitutes python syntax for java jnlp)
'''


def playGame():
    '''
    Demo area for the game. Currently, plays first 4 pieces, then fails a piece and       waits 60 seconds
    (before quitting)
    '''
    while(True):
        gamemapstate=GameMapState()
        remainingpiecesstate=RemainingPiecesState()
        turnstate=TurnState()
        g=GameGUI()
        piecelistgui=PieceListGUI(remainingpiecesstate)
        gm=GameMapGUI(g)
        gc=GameController()
        vc=ScorePaneGUI()
        gm.refresh(gamemapstate)
        timeout=60000
        sleeptime=100
        while(turnstate.keepPlaying()):
            turntime=timeout
            vc.refresh(remainingpiecesstate.score1, remainingpiecesstate.score2, remainingpiecesstate.score3, remainingpiecesstate.score4, turntime/1000)
            if not gc.demoMode:
                playerID=turnstate.currentPlayer
                piecelistgui.setActiveTab(playerID)
                while((not gc.demoMode) and (turntime>0)):
                    if (turntime%1000)==0:
                        vc.refreshtime(turntime/1000)
                        notify("Time Remaining for current player:%d seconds"%(turntime/1000)) #Comment out so that the log is readable
                    playerID=turnstate.currentPlayer
                    piecelistgui.setActivePlayer(playerID)
                    if playTurnGUI(gamemapstate,turnstate,remainingpiecesstate):
                        gm.refresh(gamemapstate)
                        piecelistgui.refresh(remainingpiecesstate, playerID)
                        piecelistgui.setActiveTab(turnstate.currentPlayer)
                        turntime=timeout
                    else:
                        turntime=turntime-sleeptime
                        Thread.sleep(sleeptime)
                if bruteAI(gamemapstate,remainingpiecesstate,turnstate,gm,piecelistgui)>0:
                    pass
                else:
                    turnstate.passTurn()
            else:
                if bruteAI(gamemapstate,remainingpiecesstate,turnstate,gm,piecelistgui)>0:
                    piecelistgui.setActiveTab(turnstate.currentPlayer)
                    #piecelistgui.setActivePlayer(turnstate.currentPlayer)
                else:
                    turnstate.passTurn()
                Thread.sleep(1000) #Commented out for speed
        vc.gameOver()
        Thread.sleep(10000) # Change this for longer pause at the end of the game
        gm.cleanUp()
        piecelistgui.cleanUp()
        gc.cleanUp()
        vc.cleanUp()
    return

playGame()
