import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.zip.ZipFile;
import org.python.core.PyStringMap;
import org.python.core.PyCode;
import org.python.core.Py;
import org.python.core.CompileMode;
import org.python.core.PyString;
import org.python.core.PySystemState;


/**
 * Ryan Attard
 */

public class Main {


    public static void main(String[] args) {
        PySystemState.initialize();
        // Load and run the startup module
        runResource("__run__.py");

    }
    public static void runResource(String name) {
        try {
            PyStringMap locals = new PyStringMap();
            locals.__setitem__("__name__",new PyString("__main__"));
            InputStream file = Main.class.getResourceAsStream("/" + name);
            PyCode code;
            CompileMode cm= CompileMode.getMode("exec");
            try {
                code = Py.compile(file, name, cm);
            } finally {
                file.close();
            }

            Py.runCode(code, locals, locals);
        }
        catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }
}
