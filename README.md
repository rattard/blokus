##This is Pipe Softworks implementation of Blokus.
##CS487
## IIT Spring 2013
## Created by Ryan Attard
##[git.blokus.ryanattard.info](http://git.blokus.ryanattard.info)
##[launch.blokus.ryanattard.info](http://launch.blokus.ryanattard.info)
##[doc.blokus.ryanattard.info](http://doc.blokus.ryanattard.info)
I'm currently being extremely overzealous in my run and build options.

You have at least 5 options for running the file.

If you're developing, I highly recommend using 'make firstrun',
which installs the dependencies (jython, epydoc) into the correct 
locations I arbitrarily deemed fit. 

'make' from then on will build the copy the jython standalone jar (for our manipulation)
then build the jython libraries into the standalone, followed by generating the documentation with epydoc. the make command automatically invokes blokus (which I did intentionally, since I never built anything then didn't run it).

Your options to run it:

1. ./blokus from command line in repo with blokuslib.jar
2. Mac OS X Automator App
3. jython main.py from command line.
4. JNLP  [launch.blokus.ryanattard.info](http://launch.blokus.ryanattard.info))
5. Direct invocation of standalone jar and __run__.py packed into jar by command java -jar blokuslib.jar -jar blokuslib.jar


