# table_def.py
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Integer, String, TIMESTAMP, Boolean, text
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('mysql+zxjdbc://root:pipesoftworks@216.47.139.252:3306/blokus', echo=True)
Base = declarative_base()
 
########################################################################
class User(Base):
    """"""
    __tablename__ = "user"
    __table_args__ = {'mysql_engine':'InnoDB'}
 
    id = Column(Integer, primary_key=True)
    username = Column(String(45), unique=True, nullable=False)
    password = Column(String(76), nullable=False)
    real_name = Column(String(45), nullable=False)
    rank = Column(Integer, nullable=False, server_default=text(str(0)))
 
    #----------------------------------------------------------------------
    def __init__(self, username, password, real_name):
        """"""
        self.username = username
        self.password = password
        self.real_name = real_name
 
########################################################################
class Game(Base):
    """"""
    __tablename__ = "game"
    __table_args__ = {'mysql_engine':'InnoDB'}
 
    id = Column(Integer, primary_key=True)
    start_time = Column(TIMESTAMP, nullable=False, server_default=text('CURRENT_TIMESTAMP'))
    end_time = Column(TIMESTAMP)

class User_Game(Base):
    """"""
    __tablename__ = 'user_game'
    __table_args__ = {'mysql_engine':'InnoDB'}
    
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    game_id = Column(Integer, ForeignKey('game.id'), primary_key=True)
    score = Column(Integer, nullable=False, server_default=text(str(0)))
    won_lost = Column(Boolean)
    
# create tables
Base.metadata.create_all(engine)