#Pipe Softworks Blokus Tests
# Using Python Unittest
# Generated May 3 2013
import databasetest
from game import GameMapState,RemainingPiecesState,TurnState,GameMapGUI,GameGUI,PieceListGUI,bruteAI, playTurnGUI,notify,GameController,getPlayerInformationStdin

import unittest
class TestBlokus(unittest.TestCase):
# To check if it's a corners
# Owner: Ryan
# Function Being Tested Definer Ryan
    def test1(self):
        game=GameMapState()
        result=game.isCorner(1,1)
        self.assertFalse(result,"Failed")

#  To initialize game board
# Owner: Ryan
# Function Being Tested Definer Ryan

    def test4(self):
        result=main.initGameMap()
        self.assertFalse(result,"Failed")

# To add a default piece
# Owner: Ryan
# Function Being Tested Definer Ryan

    def test5(self):
        result=main.addDefaultPieces([1,2,3], 1)
        self.assertFalse(result, "Failed")

# To initialize game pieces
# Owner: Ryan
# Function Being Tested Definer Ryan

    def test6(self):
        result=main.initGamePieces()
        self.assertFalse(result, "Failed")
# Unit test to check if the game ends
# Owner: Ryan
# Function Being Tested Definer Ryan

    def test3(self):
        game=TurnState()
        result=game.endGame()
        self.assertFalse(result,"Failed")
# Unit test for turn change
# Owner: Ryan
# Function Being Tested Definer Ryan

    def test4(self):
        game=TurnState()
        result=game.incrementTurnCount()
        self.assertFalse(result,"Failed")

    def test5(self):
# Unit test for pass turn
# Owner: Ryan
# Function Being Tested Definer Ryan

        game=TurnState()
        result=game.passTurn()
        self.assertFalse(result,"Failed")

    def test6(self):
# Unit test for reset turn state after a round of player pasisng
# Owner: Ryan
# Function Being Tested Definer Ryan

        game=TurnState()
        result=game.resetPassCount()
        self.assertFalse(result,"Failed")

    def test6a(self):
# Unit test for pass turn
# Owner: Ryan
# Function Being Tested Definer Ryan

        turn=TurnState()
        turn.currentPlayer=4
        turn.firstPass=0
        turn.numPasses=0
        turn.turnCount=1
        result=turn.incrementCurrentPlayer()
        self.assertTrue(result,"Failed")

    def test6b(self):
# Unit test for continuing to play if there are remaining plays on the board
# Owner: Ryan
# Function Being Tested Definer Ryan

        turn=TurnState()
        turn.currentPlayer=1
        turn.firstPass=0
        turn.numPasses=0
        turn.turnCount=1
        result=turn.keepPlaying()
        self.assertTrue(result,"Failed")


    def test19(self):
# To add piece
# Owner: Ricco
# Function Being Tested Definer Ricco

        game=RemainingPiecesState()
        result=game.rotate("left",1)
        self.assertFalse(result,"Failed")

    def test2(self):
# To print gamemap
# Owner: Ryan
# Function Being Tested Definer Ryan

        game=GameMapState()
        result=game.printGameMapStdout()
        self.assertFalse(result,"Failed")
    def testGameDBCreate(self):
# Check if the game can be created in the DB
# Owner: Ryan
# Function Being Tested Definer Jason
        result=databasetest.Game()
        self.assertFalse(result,"Failed")
    def testGameDBCreate(self):
# Check if the game can be created in the DB
# Owner: Jason
# Function Being Tested Definer Jason
        result=databasetest.Game()
        self.assertFalse(result,"Failed")
    def testGameDBCreate(self):
# Check if the UserGame can be created in the DB
# Owner: Jason
# Function Being Tested Definer Jason
        result=databasetest.User_Game()
        self.assertTrue(result,"Failed")
# Check if the User can be created in the DB
# Owner: Ryan
# Function Being Tested Definer Jason
        result=databasetest.User("hello","world","Hello World")
        self.assertTrue(result,"Failed")
if __name__ == '__main__':
    unittest.main()
