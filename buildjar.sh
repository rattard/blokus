#!/bin/bash
#Set the location for Jython libaries and working directory
CURR_DIR=`pwd`
cd $JYTHON_HOME
#echo $JYTHON_HOME
# Copy original Jython executable to new standalone
sudo cp jython.jar jythonlib.jar
# Copy all jython Libraries to the new standalone
sudo zip -r jythonlib.jar Lib 
cd  $CURR_DIR
rm -f blokuslib.jar
# Copy new all-libaries-included execuatble to generated standalone
cp $JYTHON_HOME/jythonlib.jar blokuslib.jar
sudo chmod 777 blokuslib.jar
# Copy in game and main code
zip blokuslib.jar game.py
zip blokuslib.jar __run__.py
sudo javac -classpath blokuslib.jar Main.java
zip blokuslib.jar Main.class 
zip -r blokuslib.jar images/ 
