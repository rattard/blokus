'''

Created on Feb 9, 2013

@author: Ryan Attard
blokus.ryanattard.info
CS487
"Pipe Softworks"
http://www.jython.org/jythonbook/en/1.0/LangSyntax.html
http://www.jython.org/jythonbook/en/1.0/GUIApplications.html
http://www.jython.org/jythonbook/en/1.0/TestingIntegration.html#python-testing-tools
http://www.jython.org/jythonbook/en/1.0/JythonIDE.html

Ryan is currently developing using Eclipse, Pydev, and Jython 2.7B1.
2.7B1 inclues necessary bug fixes.

For developers:
    make firstrun
    --Installs epydoc, jython into correct directories
    --builds jar
    --runs jar
    make clean
    --removes jar, class files
    --rebuild jar
    --run jar
    make
    --runs jar after rezipping files

For users:
Launch using Java Web Start:
    http://www.launch.blokus.ryanattard.info
Or:
    run using "Blokus.app" on OS X
Or:
    run using ./blokus
Or:
    invoke jython directly via:
        >jython main.py
Or:
    invole jython via .jar, bootstrapping itself on itself
        >java -jar blokuslib.jar -jar blokuslib.jar
'''
from java.lang import Math
from java.lang import Integer
from java.lang import String
PLAYERLIST=["1","2","3","4"]
PLAYERCOUNT=4 #Number of players actually playing. Game structure as defined by rules determines 4 piece sets must be used at all times
MAXPIECELENGTH=5 #Width of the largest piece
RESOLUTIONW=1024
GAMEMAPW=600 #Used to be actual screenw, now just used for GameMapGUI sizing
GAMEMAPH=600 #same as screenw
PIECELISTDIMENSION=GAMEMAPW
MAPSIZE=20
SLEEPTIME=1  #Time in secs to sleep
BUTTONSIZE=GAMEMAPH/MAPSIZE #This has been deprecated by a single const in a function, but should be revisited. 128px is the max we should ever need
ICONSIZE="128"
ICONPATH="images/icons/1_Desktop_Icons/%s%s.png"
LAUNCHSITEURL="http://launch.blokus.ryanattard.info/"
SPLASHSCREENPATH="images/screenshots/blokusboardplayed.png"
VERBOSE=True #Old print statements were blindly refractored to a new "notify" method, to be switched on/off
DEMOMODE=False
PIECESELECTED=None
LOCATIONSELECTED=None
#Game Map is a dictionary taking a string of "X,Y" as the key and the player ID as a
#Player Pieces are a list of mini-gamemaps
DEFAULTPIECECOORDSLIST=[["0,0"],["0,0","0,1"],["0,0","0,1","0,2"],["0,0","0,1","1,1"],
                        ["0,0","0,1","1,1","1,0"],["0,0","0,1","0,2","1,1"],
                        ["0,0","0,1","0,2","0,3"],["0,0","0,1","0,2","1,2"],
                        ["0,0","0,1","1,1","1,2"],["0,0","0,1","1,1","2,1","3,1"],
                        ["0,0","0,1","0,2","1,1","2,1"],["0,0","0,1","0,2","1,2","2,2"],
                        ["0,0","0,1","1,1","1,2","1,3"],["0,0","1,0","1,1","1,2","2,2"],
                        ["0,0","0,1","0,2","0,3","0,4"],["0,0","0,1","0,2","1,1","1,2"],
                        ["0,0","1,0","1,1","2,1","2,2"],["0,0","1,0","2,0","0,1","2,1"],
                        ["1,0","2,0","0,1","1,1","1,2"],["1,0","0,1","1,1","2,1","1,2"],
                        ["0,0","0,1","1,1","0,2","0,3"]
                        ]

DEFAULTPIECECOORDSLIST2=[["0,0"],["0,0","1,0"],["0,0","1,0","2,0t"],["0,0","1,0","0,1"],
                         ["0,0","0,1","1,1","1,0"],["0,0","1,0","2,0","1,1"],
                         ["0,0","1,0","2,0","3,0"],["0,0","1,0","2,0","0,1"],
                         ["1,0","2,0","0,1","1,1"],["0,0","1,0","0,1","0,2","0,3"],
                         ["0,0","1,0","2,0","1,1","1,2"],["0,0","1,0","2,0","0,1","0,2"],
                         ["2,0","3,0","0,1","1,1","2,1"],["2,0","0,1","1,1","2,1","0,2"],
                         ["0,0","1,0","2,0","3,0","4,0"],["0,0","1,0","2,0","0,1","1,1"],
                         ["2,0","1,1","2,1","0,2","1,2"],["0,0","1,0","1,1","0,2","1,2"],
                         ["1,0","0,1","1,1","2,1","2,2"],["1,0","0,1","1,1","2,1","1,2"],
                         ["0,0","1,0","2,0","3,0","2,1"]
                         ]
DEFAULTPIECECOORDSLIST3=[["0,0"],["0,0","0,1"],["0,0","0,1","0,2"],["0,0","1,0","1,1"],
                         ["0,0","0,1","1,1","1,0"],["1,0","0,1","1,1","1,2"],
                         ["0,0","0,1","0,2","0,3"],["0,0","1,0","1,1","1,2"],
                         ["0,0","0,1","1,1","1,2"],["0,0","1,0","2,0","3,0","3,1"],
                         ["2,0","0,1","1,1","2,1","2,2"],["0,0","1,0","2,0","2,1","2,2"],
                         ["0,0","0,1","0,2","1,2","1,3"],["0,0","1,0","1,1","1,2","2,2"],
                         ["0,0","0,1","0,2","0,3","0,4"],["0,0","1,0","0,1","1,1","1,2"],
                         ["0,0","0,1","1,1","1,2","2,2"],["0,0","2,0","0,1","1,1","2,1"],
                         ["1,0","1,1","2,1","0,2","1,2"],["1,0","0,1","1,1","2,1","1,2"],
                         ["1,0","1,1","0,2","1,2","1,3"]
                         ]

DEFAULTPIECECOORDSLIST4=[["0,0"],["0,0","1,0"],["0,0","1,0", "2,0"],["1,0","0,1","1,1"],
                         ["0,0","1,0","1,1","0,1"],["0,1","1,0","1,1","2,1"],
                         ["0,0","1,0","2,0","3,0"],["2,0","0,1","1,1","2,1"],
                         ["1,0","2,0","0,1","1,1"],["1,0","1,1","1,2","0,3","1,3"],
                         ["1,0","1,1","0,2","1,2","2,2"],["2,0","2,1","0,2","1,2","2,2"],
                         ["1,0","2,0","3,0","0,1","1,1"],["2,0","0,1","1,1","2,1","0,2"],
                         ["0,0","1,0","2,0","3,0","4,0"],["1,0","2,0","0,1","1,1","2,1"],
                         ["1,0","2,0","0,1","1,1","0,2"],["0,0","1,0","0,1","0,2","1,2"],
                         ["0,0","0,1","1,1","2,1","1,2"],["1,0","0,1","1,1","2,1","1,2"],
                         ["1,0","0,1","1,1","2,1","3,1"]
                         ]

 



DEFAULTPIECECOUNT=DEFAULTPIECECOORDSLIST.__len__()

#PERSONID holds an array indexed by the "CURRENTPLAYER" variable, that returns the ID of the actual person playing
#0 is computer?
#PERSONID[1;2] are players 1 and 2 (always filled)
#PERSONID[3,4] are either actual players or refer to Players 1 and 2. By default they're their own players
PERSONID=[0,1,2,3,4]
#Whether or not to use GUI
GUI=True
#Directly correlating with the values of the game map
COLORS=["black","blue","yellow","red","green"]

class TurnState():
    '''
    Maintain who is currently up to play a piece, and what turn the game is on.
    '''
    turnCount=1
    currentPlayer=1
    numPasses=0
    firstPass=0
    def __init__(self):
        pass
    def incrementTurnCount(self):
        '''
        Increase the turn count by 1.
        '''
        self.turnCount=self.turnCount+1
    def incrementCurrentPlayer(self):
        '''
        Increment the current player. If the current player is the last for a round,
        then increment the turncount and return to player 1.
        Passsing is also kept track of here, in case of final round where all four pass consecutively.
        '''
        if(self.currentPlayer<4):
            self.currentPlayer=self.currentPlayer+1
            return True
        elif(self.currentPlayer==4):
            self.currentPlayer=1
            self.incrementTurnCount()
            return True
    def resetPassCount(self):
        self.numPasses=0
        self.firstPass=0
    def advanceTurn(self):
        self.incrementCurrentPlayer()
        self.resetPassCount()
    def passTurn(self):
        '''
        Pass current player's turn
        '''
        if(self.firstPass==0):
            self.firstPass=self.currentPlayer
        self.numPasses=self.numPasses+1
        self.incrementCurrentPlayer()
        if(self.numPasses==4):
            self.endGame()
    def keepPlaying(self):
        if(self.numPasses<4):
            return True
        return False
    def endGame(self):
        notify( "GAME OVER!!!!")
        print 'Player 1 Score:',RemainingPiecesState.score1
        print 'Player 2 Score: ',RemainingPiecesState.score2
        print 'Player 3 Score:',RemainingPiecesState.score3
        print 'Player 4 Score: ',RemainingPiecesState.score4
        ##   min=RemainingPiecesState.score1
        scorelist=[RemainingPiecesState.score1,RemainingPiecesState.score2,RemainingPiecesState.score3,RemainingPiecesState.score4]
        
        print 'Minimum Score:',min(scorelist)
        
        if RemainingPiecesState.score1==min(scorelist):
            print 'Winner is Player 1'     
        if RemainingPiecesState.score2==min(scorelist):
            print 'Winner is Player 2'     
        if RemainingPiecesState.score3==min(scorelist):
            print 'Winner is Player 3'       
        if RemainingPiecesState.score4==min(scorelist):
            print 'Winner is Player 4'

class GameMapState():
    '''
    Maintain game state in a dict for the gamemap
    '''
    gamemap={}
    mapsize=MAPSIZE
    def __init__(self):
        for i in range (0,self.mapsize):
            for j in range(0,self.mapsize):
                coords=coordsToString(j,i)
                self.gamemap[coords]=0
    def printGameMapStdout(self):
        '''
        Print out the game map via stdout
        '''
        printDictStdout(self.gamemap,MAPSIZE)
        return

    def playPiece(self,remainingpieces,xcoord,ycoord,piece,playerID,turnstate,pieceOffsetX=None, pieceOffsetY=None):
        '''
        Play a piece according to game rules, and modify remaining pieces and turnstate accordingly if the play is valid.
        '''
        def addPieceToBoard(xcoord,ycoord,piece):
            '''
            Add a piece directly: no verification, only called inside an if statement with clause isValidPlay(...)
            '''
            for i in range(MAXPIECELENGTH):
                for j in range(MAXPIECELENGTH):
                    if piece[coordsToString(j,i)]>0:
                        self.gamemap[coordsToString(xcoord+j,ycoord+i)]=piece[coordsToString(j,i)]
        if playerID!=turnstate.currentPlayer:
            notify("Not the correct player's piece for this turn!")
            return False
        if self.isValidPlay(xcoord, ycoord, piece,playerID,turnstate.turnCount):
            notify( "valid play at %i,%i, player %i"%(xcoord,ycoord,playerID))
            addPieceToBoard(xcoord,ycoord,piece)
            remainingpieces.removePiece(playerID,piece)
            remainingpieces.score(piece,playerID)
            turnstate.advanceTurn()
            return True
        notify( "invalid play at %i,%i, player %i"%(xcoord,ycoord,playerID))
        return False
    def isCorrectPlayersPiece(self,piece,currentplayer):
        for i in range(MAXPIECELENGTH):
            for j in range(MAXPIECELENGTH):
                if piece[coordsToString(j,i)] !=0:
                    if piece[coordsToString(j,i)] !=currentplayer:
                        print "wrong player's piece"
                        return False
        print "correct player's piece"
        return True
    def isValidPlay(self,xbase,ybase,piece,currentplayer,turncount):
        '''
        verify the piece is not violating direct game rule
        '''
        if not self.isCorrectPlayersPiece(piece, currentplayer):
            notify("incorrect player's piece")
            return False
        if self.isOverlap(xbase,ybase,piece):
            notify("is overlap edge of board or other piece at %i,%i, player %i"%(xbase,ybase,currentplayer))
            return False
        if self.isTouchingOwnEdge(xbase,ybase,piece,currentplayer):
            notify("touching own edge at %i,%i, player %i"%(xbase,ybase,currentplayer))
            return False
        if not self.isTouchingOwnCorner(xbase,ybase,piece,currentplayer,turncount):
            notify("not touching own corner at %i,%i, player %i"%(xbase,ybase,currentplayer))
            return False
        return True

    def isOverlap(self,xbase,ybase,piece):
        '''
        verify the piece is not overlapping a currently occupied tile
        '''
        for i in range(MAXPIECELENGTH):
            for j in range(MAXPIECELENGTH):
                #if the piece is actually using that part of the gamemap
                if piece[coordsToString(j,i)]>0:
                    xcoord=j+xbase
                    ycoord=i+ybase
                    #direct violation by overlap with game boundary
                    if (xcoord>=MAPSIZE or ycoord>=MAPSIZE):
                        notify( "out of bounds %i,%i"%(xcoord,ycoord))
                        return True
                    if self.gamemap[coordsToString((xcoord),(ycoord))]>0:
                        notify( "overlap existing tile")
                        return True
        return False

    def isTouchingOwnEdge(self,xbase,ybase,piece,playerID):
        '''
        Verify piece being played doesn't touch the existing edge
        '''

        for i in range(MAXPIECELENGTH):
            for j in range(MAXPIECELENGTH):
                xcoord=j+xbase
                ycoord=i+ybase
                if (xcoord<MAPSIZE and ycoord<MAPSIZE and piece[coordsToString(j,i)]>0):
                    if(xcoord-1>0 and self.gamemap[coordsToString((xcoord-1),(ycoord))]==playerID):
                        return True
                    if(xcoord+1<MAPSIZE and self.gamemap[coordsToString((xcoord+1),(ycoord))]==playerID):
                        return True
                    if(ycoord-1>0 and self.gamemap[coordsToString((xcoord),(ycoord-1))]==playerID):
                        return True
                    if(ycoord+1<MAPSIZE and self.gamemap[coordsToString((xcoord),(ycoord+1))]==playerID):
                        return True
        return False

    def isTouchingOwnCorner(self,xbase,ybase,piece,playerID,turncount):
        '''
        Verify piece being played touches a corner of the previously played pieces
        '''
        for i in range(MAXPIECELENGTH):
            for j in range(MAXPIECELENGTH):
                #if the piece is actually using that part of the gamemap
                if piece[coordsToString(j,i)]>0:
                    xcoord=j+xbase
                    ycoord=i+ybase
                    if(turncount==1):
                        if self.isCorner(xcoord,ycoord):
                            notify( coordsToString(xcoord, ycoord)+" is a corner." )
                            return True
                    if(xcoord-1>=0 and ycoord-1>=0 and
                       self.gamemap[coordsToString((xcoord-1),(ycoord-1))]==playerID):
                        return True
                    if(xcoord+1<MAPSIZE and ycoord-1>=0 and
                       self.gamemap[coordsToString((xcoord+1),(ycoord-1))]==playerID):
                        return True
                    if(xcoord-1>=0and ycoord+1<MAPSIZE and
                       self.gamemap[coordsToString((xcoord-1),(ycoord+1))]==playerID):
                        return True
                    if(xcoord+1<MAPSIZE and ycoord+1<MAPSIZE and
                       self.gamemap[coordsToString((xcoord+1),(ycoord+1))]==playerID):
                        return True

        return False
    def isCorner(self,xcoord,ycoord):
        '''
        Return true if the coordinate is a corner of the game map
        '''
        if(xcoord==0 and ycoord==0):
            return True
        if(xcoord==0 and ycoord==(MAPSIZE-1)):
            return True
        if(xcoord==(MAPSIZE-1) and ycoord==0):
            return True
        if(xcoord==(MAPSIZE-1) and ycoord==(MAPSIZE-1)):
            return True
        return False
class RemainingPiecesState():
    '''
    Maintain Player Pieces states together in a list of lists
    '''
    color1pieces= None
    color2pieces= None
    color3pieces= None
    color4pieces= None
    playerpiecelist=[color1pieces,color2pieces,color3pieces,color4pieces]
    score1=89
    score2=89
    score3=89
    score4=89


    def __init__(self):
        for i in range(1,5):
            self.playerpiecelist[i-1] = PiecesState(i)


    def removePiece(self,playerID,piece):     
        ##self.score(piece,playerID) 
        self.playerpiecelist[playerID-1].removePiece(piece)
        return
    
    def score(self,piece,playerID):
        count = 0
        for k, v in piece.iteritems():
            if v == playerID:
                count = count+1
                print k, v
        
        if playerID==1:
            RemainingPiecesState.score1 = RemainingPiecesState.score1 - count
        if playerID==2:
            RemainingPiecesState.score2 = RemainingPiecesState.score2 - count
        if playerID==3:
            RemainingPiecesState.score3 = RemainingPiecesState.score3 - count  
        if playerID==4:
            RemainingPiecesState.score4 = RemainingPiecesState.score4 - count
                 
    def getPlayerPieces(self,playerID):
        return self.playerpiecelist[playerID-1].getCurrentPieces()

    def pieceFromCoordsList(self,coordList,playerid):
        '''
        Take a list of coordinates of blocks and make a return a piece following those coords
        '''
        piece={}
        for i in range(coordList.__len__()):
            piece[coordList[i]]=playerid
        piece=self.fillEmptySpaces(piece)
        return piece

    def fillEmptySpaces(self,piece):
        '''
        Fill the dict with empty spaces after the pieceFromCoordsList function
        '''
        for i in range(MAXPIECELENGTH):
            for j in range(MAXPIECELENGTH):
                #Note the j,i not i,j. This is because you go row-wise
                if not piece.has_key(coordsToString(j,i)):
                    #Note the j,i not i,j. This is because you go row-wise
                    piece[coordsToString(j,i)]=0
        return piece

    def rotate(self, direction, playerID):
        '''
        Rotate a piece to be only temporarily used, not updated in the piecelist.
        '''
        if(direction == "left"):
            notify("Rotated player %d's pieces to the left" % playerID)
            self.playerpiecelist[playerID-1].rotateStateLeft()
        elif(direction == "right"):
            notify("Rotated player %d's pieces to the right" % playerID)
            self.playerpiecelist[playerID-1].rotateStateRight()


    def isValidPiece(self,piece):
        numTiles=0
        numFails=0
        for i in range(MAXPIECELENGTH):
            for j in range(MAXPIECELENGTH):
                if piece[coordsToString(j,i)]>0:
                    numTiles=numTiles+1
                    numEdges=0
                    #Make your eight neighbor comparisons if it's not the first tile
                    if(j-1>=0 and piece[coordsToString(j-1,i)]>0):
                        numEdges=numEdges+1
                    if (j+1<MAXPIECELENGTH and piece[coordsToString(j+1,i)]>0):
                        numEdges=numEdges+1
                    if (i-1>=0 and piece[coordsToString(j,i-1)]>0):
                        numEdges=numEdges+1
                    if (i+1<MAXPIECELENGTH and piece[coordsToString(j,i+1)]>0):
                        numEdges=numEdges+1
                    if(j-1>=0 and i-1>=0 and piece[coordsToString(j-1,i-1)]>0):
                        numEdges=numEdges+1
                    if(j+1<MAXPIECELENGTH and i-1>=0 and piece[coordsToString(j+1,i-1)]>0):
                        numEdges=numEdges+1
                    if(j-1>=0 and i+1<MAXPIECELENGTH and piece[coordsToString(j-1,i+1)]>0):
                        numEdges=numEdges+1
                    if(j+1<MAXPIECELENGTH and i+1<MAXPIECELENGTH and piece[coordsToString(j+1,i+1)]>0):
                        numEdges=numEdges+1
                    if(numEdges==0):
                        numFails=numFails+1
        if numFails>0 and numTiles>1:
            return False
        else:
            return True

    #List Remaining Pieces
    def numPiecesRemaining(self,playerID):
        '''
        Return the number of elements remaining in the players piece list
        '''

        return self.playerpiecelist[playerID-1].getCurrentPieces().__len__()

    def printPlayerPiecesStdout(self,pieces):
            for i in range(pieces.__len__()):
                notify( "Piece #%i"%(i) )
                printDictStdout(pieces[i])

class PiecesState():
    # Used for rotation

    def __init__(self, playerID):
        self.playerID = playerID
        self.maxStates = 4
        self.currentState = 0
        self.piecelist1 = []
        self.piecelist2 = []
        self.piecelist3 = []
        self.piecelist4 = []
        self.pieceStates = {0:self.piecelist1, 1:self.piecelist2, 2:self.piecelist3, 3:self.piecelist4}
        self.init()

    def init(self):
        # First list
        coordslist=DEFAULTPIECECOORDSLIST
        for pieceCoords in coordslist:
            self.piecelist1.append(self.pieceFromCoordsList(pieceCoords,self.playerID))    
        
        # Second list
        coordslist=DEFAULTPIECECOORDSLIST2
        for pieceCoords in coordslist:
            self.piecelist2.append(self.pieceFromCoordsList(pieceCoords,self.playerID))
    
        # Third list
        coordslist=DEFAULTPIECECOORDSLIST3
        for pieceCoords in coordslist:
            self.piecelist3.append(self.pieceFromCoordsList(pieceCoords,self.playerID))
        # Fourth list
        coordslist=DEFAULTPIECECOORDSLIST4
        for pieceCoords in coordslist:
            self.piecelist4.append(self.pieceFromCoordsList(pieceCoords,self.playerID))
        return

    def getCurrentPieces(self):
        return(self.pieceStates[self.currentState])

    def rotateStateRight(self):
        self.currentState = (self.currentState + 1) % self.maxStates

    def rotateStateLeft(self):
        self.currentState = (abs(self.currentState - 1)) % self.maxStates

    def removePiece(self, piece):
        index = self.pieceStates[self.currentState].index(piece)
        for i in range(0,self.maxStates):
            self.pieceStates[i].pop(index) # Remove that piece for all states

    def pieceFromCoordsList(self,coordList,playerid):
        '''
        Take a list of coordinates of blocks and make a return a piece following those coords
        '''
        piece={}
        for i in range(coordList.__len__()):
            piece[coordList[i]]=playerid
        piece=self.fillEmptySpaces(piece)
        return piece

    def fillEmptySpaces(self,piece):
        '''
        Fill the dict with empty spaces after the pieceFromCoordsList function
        '''
        for i in range(MAXPIECELENGTH):
            for j in range(MAXPIECELENGTH):
                #Note the j,i not i,j. This is because you go row-wise
                if not piece.has_key(coordsToString(j,i)):
                    #Note the j,i not i,j. This is because you go row-wise
                    piece[coordsToString(j,i)]=0
        return piece





from javax.swing import JFrame,JButton,WindowConstants,ImageIcon,JTabbedPane,AbstractAction,BorderFactory,JLabel,JPanel,BoxLayout,JInternalFrame, Box
from java.awt import GridLayout,CardLayout,Color,Image, BorderLayout, Dimension, FlowLayout, Component


from java.net import URL
from java.lang import ClassLoader
from javax.imageio import ImageIO
from java.lang import Thread
from java.lang import System
class GameGUI:
    '''
    Intended to be the GUI controller, a place to load the required image files, refresh the whole GUI via one call, and manage actual gameplay in a different class

    '''
    def __init__(self):
        SplashScreenGUI(self)

    def clickSplashScreen(self):
        self.splashscreeniconclicked=True

class SplashScreenGUI:
    '''
    GUI Implementation of a splashscreen. Real exciting work done here.
    '''
    def __init__(self,gamegui):
        frame=JFrame("Blokus SplashScreen")
        frame.setLayout(BorderLayout())
        frame.setContentPane(JLabel(ImageIcon(self.loadSplashImage())))
        frame.setLayout(FlowLayout())

        onclick=SplashScreenActionGUI()

        buttonPanel = JPanel()
        buttonPanel.setLayout(BoxLayout(buttonPanel, BoxLayout.Y_AXIS))
        startGameButton = JButton()
        startGameButton.setAction(onclick)
        startGameButton.setText("Start Game")
        startGameButton.setPreferredSize(Dimension(100,50))
        startGameButton.setAlignmentX(Component.CENTER_ALIGNMENT)
        endGameButton = JButton (actionPerformed=self.onQuit)
        endGameButton.setText("Quit Game")
        endGameButton.setPreferredSize(Dimension(100,50))
        endGameButton.setAlignmentX(Component.CENTER_ALIGNMENT)

        logoPanel = JPanel()
        logoPanel.setLayout(FlowLayout())
        jythonLabel = JLabel(ImageIcon(self.loadImage("images/logos/jythonlogowhite2.png")))
        blokusLabel = JLabel(ImageIcon(self.loadImage("images/logos/blokuslogo.png")))
        logoPanel.add(jythonLabel)
        logoPanel.add(blokusLabel)
        pipeLabel = JLabel(ImageIcon(self.loadImage("images/logos/pipelogo2.png")))
        pipeLabel.setAlignmentX(Component.CENTER_ALIGNMENT)

        logoPanel.setOpaque(False)
        buttonPanel.add(logoPanel)
        buttonPanel.add(startGameButton)
        buttonPanel.add(Box.createRigidArea(Dimension(0,20))) #Filler space between buttons
        buttonPanel.add(endGameButton)
        buttonPanel.add(Box.createRigidArea(Dimension(0, 100)))
        buttonPanel.add(pipeLabel)
        buttonPanel.add(Box.createRigidArea(Dimension(0,100))) #End filler space

        buttonPanel.setOpaque(False)
        frame.add(buttonPanel)
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
        frame.setSize(800, 800)
        frame.setLocationRelativeTo(None)
        frame.setVisible(True)


        while(not onclick.clicked):
            Thread.sleep(100)
        frame.dispose()

    def onQuit(self, e):
        System.exit(0)

    def loadSplashImage(self):
        '''
        Loads the image for the splashscreen, and setsup the icon. Returns the icon generated so the GUI controller can listen for actions
        '''
        path=SPLASHSCREENPATH
        return self.loadImage(path)
    def loadImage(self, path):
        url=ClassLoader.getSystemClassLoader().getResource(path)
        if url==None:
            url=LAUNCHSITEURL+path
            image=ImageIO.read(URL(url))
        else:
            image=ImageIO.read(url)
            return image
class SplashScreenActionGUI(AbstractAction):
    '''
    On click action for splashscreen. Continues loading.
    '''
    clicked=False
    def __init__(self):
        pass
    def actionPerformed(self,e):
        self.clicked=True
        print "Clicked through splashscreen"
class GameMapGUI:
    '''
    A JFrame that contains the GameMapGUI.
    '''
    gametiles={}
    iconimages=[]
    frame=None
    def __init__(self,gameGUI):
        self.loadImages()
        self.frame = JFrame("Blokus")
        self.frame.setBackground(Color.black)
        self.frame.setLayout(GridLayout(MAPSIZE,MAPSIZE))
        self.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
        self.frame.setSize(GAMEMAPW, GAMEMAPH)
        for i in range(MAPSIZE):
            for j in range(MAPSIZE):
                t=TileGUI(j,i,self.iconimages[0])
                t.setVisible(True)
                self.frame.add(t)
                self.gametiles["%i,%i"%(j,i)]=t
                self.frame.repaint()
        self.frame.validate()
        self.frame.setVisible(True)
        self.frame.repaint()

    def refresh(self,gameMapState):
        '''
        Refresh the GameMapGUI after a change in the GameMapState, such as after a piece being played.
        '''
        for j in range (gameMapState.mapsize):
            for i in range(gameMapState.mapsize):
                self.gametiles[coordsToString(j, i)].setColor(self.iconimages[gameMapState.gamemap[coordsToString(j,i)]])
        self.frame.repaint()
    def loadImages(self):
        for color in COLORS:
            path=ICONPATH%(color,ICONSIZE)
            #Load the iconimages from the JAR--else load them from HTTP in case of JNLP
            url=ClassLoader.getSystemClassLoader().getResource(path)
            if url==None:
                url="http://launch.blokus.ryanattard.info/"+path
                image=ImageIO.read(URL(url))
            else:
                image=ImageIO.read(url)
            iconImage=ImageIcon(image)
            self.iconimages.append(iconImage)

    def cleanUp(self):
        self.frame.dispose()

def calculateGridSize(pieces):
    return int(Math.ceil(Math.sqrt(pieces.__len__())))

class PieceListGUI():
    '''
    A frame with tabbed panels representing the remaining piece state
    '''
    iconImages=[]
    frame=None
    playerTabs=None
    player1cards=None
    player2cards=None
    player3cards=None
    player4cards=None
    cards=[player1cards,player2cards,player3cards,player4cards]
    def __init__(self,remainingpiecesstate):
        self.remainingpiecesstate = remainingpiecesstate #Need this for rotate.
        self.frame=JFrame("Blokus Piece Viewer")
        self.frame.setLayout(BorderLayout())
        self.loadImages()
        self.frame.setSize(PIECELISTDIMENSION, PIECELISTDIMENSION+60)
        self.frame.setLocation(GAMEMAPW+1, 0)
        self.playerTabs=JTabbedPane()
        self.activePlayer = 1 # Set to player 1 by default.  Could use this for active tab...

        # Rotation components
        self.buttPanel=JPanel() # Panel to hold the left and right buttons
        self.leftButton=JButton("Left", actionPerformed = self.rotateLeftAction)
        self.leftButton.setPreferredSize(Dimension(100, 50))
        self.rightButton=JButton("Right", actionPerformed = self.rotateRightAction)
        self.rightButton.setPreferredSize(Dimension(100, 50))

        # Need to add buttons here
        for j in range (4):
            gridsize=calculateGridSize(remainingpiecesstate.playerpiecelist[j].getCurrentPieces()) # This had to be changed with getCrurrentPieces added
            card=PlayerPieceTabGUI(remainingpiecesstate.playerpiecelist[j].getCurrentPieces(),gridsize,self.iconImages)
            self.cards[j]=card
            label="%s's Pieces"%COLORS[j+1]
            self.playerTabs.add(label,card)

        self.buttPanel.add(self.leftButton)
        self.buttPanel.add(self.rightButton)

        self.frame.add(self.buttPanel, BorderLayout.PAGE_START)
        self.frame.add(self.playerTabs, BorderLayout.CENTER)
        self.frame.setVisible(True)

    def setActiveTab(self,playerID):
        self.playerTabs.setSelectedIndex(playerID-1)

    def refresh(self,remainingpiecesstate, playerID):
        '''
        Refresh the PieceList after a change in the RemainingPieceState, such as after a piece being played.
        '''
        #print "%d"%playerID
        #self.frame.repaint()
        self.cards[playerID-1].refresh(remainingpiecesstate.getPlayerPieces(playerID))
        self.frame.validate()
        self.frame.repaint()

    def loadImages(self):
        '''
        Load images from .JAR or HTTP for PieceListGUI
        '''
        for color in COLORS:
            path=ICONPATH%(color,ICONSIZE)
            #Load the iconimages from the JAR--else load them from HTTP in case of JNLP
            url=ClassLoader.getSystemClassLoader().getResource(path)
            if url==None:
                url="http://launch.blokus.ryanattard.info/"+path
                image=ImageIO.read(URL(url))
            else:
                image=ImageIO.read(url)
            iconImage=ImageIcon(image)
            self.iconImages.append(iconImage)

    def rotateLeftAction(self, e):
        self.remainingpiecesstate.rotate("left", self.activePlayer)
        self.refresh(self.remainingpiecesstate, self.activePlayer)


    def rotateRightAction(self, e):
        self.remainingpiecesstate.rotate("right", self.activePlayer)
        self.refresh(self.remainingpiecesstate, self.activePlayer)
        
    def setActivePlayer(self, playerID):
        self.activePlayer=playerID

    def cleanUp(self):
        self.frame.dispose()
        


class PlayerPieceTabGUI(JPanel):
        '''
        A JPanel containing the player pieces. Each player gets their own tab.
        '''

        images=None
        def __init__(self,playerpiecelist,gridsize,images):
            self.images=images
            self.setLayout(GridLayout(gridsize,gridsize))
            self.reload(playerpiecelist,gridsize)
            self.setBackground(Color.black)

        def refresh(self,playerpiecelist):
            '''
            Refresh the current player's tab (forces refresh on all pieces for good reason, aka when implementing rotate just refresh the gui after a rotate)
            '''
            self.removeAll()
            gridsize=calculateGridSize(playerpiecelist)
            self.reload(playerpiecelist,gridsize)
            self.setLayout(GridLayout(gridsize,gridsize))
            #always validate, then repaint after removes
            self.validate()
            self.repaint()
        def reload(self,playerpiecelist,gridsize):
                for i in range(playerpiecelist.__len__()):
                    piece=PieceGUI(playerpiecelist[i],self.images,"Piece %i"%(i),i,gridsize)
                    self.add(piece,"Piece %i"%(i))
class PieceGUI(JPanel):
        '''
        GUI Representation of a piece, to be placed on a player's piece tab
        '''
        tiles={}
        images=[]
        def __init__(self,piece,playertileimages,label,pieceID,gridsize):
            self.images=playertileimages
            #self.setTitle(label)
            self.setLayout(GridLayout(MAXPIECELENGTH,MAXPIECELENGTH))
            for i in range (MAXPIECELENGTH):
                for j in range (MAXPIECELENGTH):
                    pieceval=piece[coordsToString(j,i)]
                    tile=TileGUI(j,i,self.images[pieceval],pieceval,pieceID)
                    tile.setLocation(j, i)
                    tile.setColor(self.images[pieceval])
                    self.tiles[coordsToString(j,i)]=tile
                    self.add(tile)
            self.setSize(PIECELISTDIMENSION/gridsize,PIECELISTDIMENSION/gridsize)
            self.setBackground(Color.black)
            self.setVisible(True)

        def refresh(self,piece):
            for i in range (MAXPIECELENGTH):
                for j in range (MAXPIECELENGTH):
                    pieceval=piece[coordsToString(j,i)]
                    self.tiles[coordsToString(j,i)].setColor(self.images[pieceval])


class TileGUI(JButton):
    '''
    An individual tile on the gameboard. Either empty (black), or occupied by a player's color
    '''
    def __init__(self,x,y,color,playerID=None,pieceNumber=None):
        self.setColor(color)
        self.setLocation(x, y)
        self.setSize(128, 128)
        self.setToolTipText(coordsToString(x,y))
        self.setBorder(BorderFactory.createLineBorder(Color.white))
        if playerID:
            self.setAction(TileActionGUI(coordsToString(x,y),playerID,pieceNumber))
        else:
            self.setAction(TileActionGUI(coordsToString(x,y)))
    def setColor(self,image):
        '''
        Set the color after an action, such as a player movement (or selection...?)
        '''
        self.setIcon(image)

class TileActionGUI(AbstractAction):
    '''
    On click action for the tiles! hooray, watch the stdout for the coordinates of your clicks
    '''
    location="-1,-1"
    owner=None
    pieceNumber=None
    def __init__(self,givenlocation,playerID=None,pieceNumber=None):
        if playerID:
            self.owner=playerID
            self.pieceNumber=pieceNumber
        self.location=givenlocation
    def actionPerformed(self,e):
        if self.owner:
            print "owned by %s at %s on piece:%d"%(COLORS[self.owner],self.location,self.pieceNumber)
            selectPiece(self.owner,self.location,self.pieceNumber)
        else:
            selectPieceDestination(self.location)
            print self.location

def selectPiece(playerID,location,piecenumber):
        global PIECESELECTED
        PIECESELECTED=[playerID,location,piecenumber]
def selectPieceDestination(location):
        global LOCATIONSELECTED
        LOCATIONSELECTED=location
def coordsToString(x,y):
    '''
    Return a string 'x,y' to be used in game map and piece dicts from two ints

    "%i,%i"%(x,y)
    '''
    string="%i,%i" % (x,y)
    return (string)
def stringToCoords(coords):
    '''
    Return a pair of ints x,y from a string label

    There's no whitespace, just %i,%i
    '''
    coordlist=String.split(coords, ',')
    return (coordlist[0],coordlist[1])
def bruteAI(gameMapState,gamePieces,turn,gameMapGUI,pieceListGUI):
    '''
    Brute force your way through the current player's pieces remaining from longest to shortest from 0,0 to 19,19
    '''
    playerID=turn.currentPlayer
    for i in range(4):
        numrem=gamePieces.numPiecesRemaining(turn.currentPlayer)
        while(numrem>0):
            for i in range(MAPSIZE):
                    for j in range(MAPSIZE):
                        if gameMapState.playPiece(gamePieces,j,i,gamePieces.getPlayerPieces(turn.currentPlayer)[numrem-1],turn.currentPlayer,turn):
                            gameMapGUI.refresh(gameMapState)
                            pieceListGUI.refresh(gamePieces,playerID)
                            return numrem-1
            numrem=numrem-1
        gamePieces.rotate("right",playerID)
    return -1
def playTurnGUI(gamemapstate,turnstate,remainingpiecesstate):
    if PIECESELECTED and LOCATIONSELECTED:
        print "selected!!"
        playerID=PIECESELECTED[0]
        xcoord=int(stringToCoords(LOCATIONSELECTED)[0])
        print "xcoord %d"%xcoord
        ycoord=int(stringToCoords(LOCATIONSELECTED)[1])
        print "ycoord %d"%ycoord
        pieceID=PIECESELECTED[2]
        print playerID
        print turnstate.currentPlayer
        piece=remainingpiecesstate.getPlayerPieces(playerID)[pieceID]
        if gamemapstate.playPiece(remainingpiecesstate,xcoord,ycoord,piece,playerID,turnstate) >0:
            clearSelections()
            return True
        else:
            clearSelections()
    return False
#    def playPiece(self,remainingpieces,xcoord,ycoord,piece,playerID,turnstate,pieceOffsetX=None, pieceOffsetY=None):
def clearSelections():
        global PIECESELECTED
        global LOCATIONSELECTED
        PIECESELECTED=None
        LOCATIONSELECTED=None

def printDictStdout(piece,piecelength=None):
        '''
        Print a piece or map with a nice formatting on the cmdline
        '''
        if piecelength==None:
            piecelength=MAXPIECELENGTH
        labelw=int(Math.ceil(Math.log10(piecelength)+2))
        #Title Line with Number Indicies
        line="".rjust(labelw)
        for i in range(piecelength):
            line=line+(("%i " % (i)).rjust(labelw-1))
        notify(line)
        #Offset for 2 digit numbers
        line="".rjust(labelw)
        for i in range(piecelength):
            line=("%i "% (i)).rjust(labelw)
            for j in range(piecelength):
                #Note the j,i not i,j. This is because you go row-wise
                line=line+(("%s "% (piece[coordsToString(j,i)])).rjust(labelw-1))
            notify(line)
        endline=""
        for i in range(line.__len__()):
            endline=endline+"-"
        notify(endline)
        return

def getPlayerInformationStdin():
    '''
    Get player information stdin. Mostly structures how player data could be saved later
    '''
    playerCountEntered=Integer.getInteger(raw_input("Enter number of players(2-4):").strip())
    while playerCountEntered > 4 or playerCountEntered < 2:
        playerCountEntered=Integer.getInteger(raw_input("Enter number of players(2-4):").strip())
    globals()['PLAYERCOUNT']=playerCountEntered
    #clear default playerlist or restore settings for individual games
    while PLAYERLIST.__len__()>0:PLAYERLIST.pop();
    for i in range (1 ,playerCountEntered+1):
        #Steal away the newline
        PLAYERLIST.append(raw_input("Enter the your name player %i:"%(i)))
    if PLAYERCOUNT==2:
        PERSONID[3]=1
        PERSONID[4]=2
        notify( "%s will play for the 3rd person, %s will play last"%(PLAYERLIST[0],PLAYERLIST[1]))
    elif PLAYERCOUNT==3:
        PERSONID[3]=1
        notify( "%s gets the extra plays by default"%(PLAYERLIST[1]))

def notify(string):
    '''
    If VERBOSE flag set, print messages logged with notify function to stdout. Could redirect to a file later.
    '''
    if VERBOSE:
        print string

class GameController():
    frame=None
    demoMode=False
    def __init__(self):
        self.frame=JFrame()
        self.frame.setSize(100, 100)
        self.frame.setLocation(0, GAMEMAPH)
        DemoButton=JButton()
        #DemoButton.setSize(10, 10)
        label=JLabel("Demo Mode")
        DemoButton.add(label)
        DemoButton.setVisible(True)
        DemoButton.setSize(60, 60)
        action=DemoButtonAction(self)
        DemoButton.setAction(action)
        self.frame.add(DemoButton)
        self.frame.setVisible(True)
        self.frame.repaint()

    def cleanUp(self):
        self.frame.dispose()

class DemoButtonAction(AbstractAction):
        GameController=None
        def __init__(self,gamecontroller):
            self.GameController=gamecontroller
        def actionPerformed(self,e):
            self.GameController.demoMode=not self.GameController.demoMode

class ScorePaneGUI():
    frame=None
    scoretabs=None
    statuspane=None
    statusmsg=None
    score1panel=None
    score1label=None
    score2panel=None
    score2label=None
    score3panel=None
    score3label=None
    score4panel=None
    score4label=None
    def __init__(self):
        self.frame=JFrame()
        self.frame.setSize(300, 200)
        self.frame.setLocation(130, GAMEMAPH)
        self.frame.setLayout(BorderLayout())
        #DemoButton.setSize(10, 10)
        self.scoretabs=JTabbedPane()
        self.score1panel=JPanel()
        self.score4panel=JPanel()
        self.score2panel=JPanel()
        self.score3panel=JPanel()

        self.score1label=JLabel("Score Pane! SEE A SCORE")
        self.score2label=JLabel("Score Pane! SEE A SCORE2")
        self.score3label=JLabel("Score Pane! SEE A SCORE3")
        self.score4label=JLabel("Score Pane! SEE A SCORE4")
        self.score1panel.add(self.score1label)
        self.score2panel.add(self.score2label)
        self.score3panel.add(self.score3label)
        self.score4panel.add(self.score4label)
        self.scoretabs.add("Player 1's Scores",self.score1panel)
        self.scoretabs.add("Player 2's Scores",self.score2panel)
        self.scoretabs.add("Player 3's Scores",self.score3panel)
        self.scoretabs.add("Player 4's Scores",self.score4panel)
        self.statuspane=JPanel()
        self.statusmsg=JLabel("Game Status Messages Appear Here.")
        self.statuspane.add(self.statusmsg)
        self.frame.add(self.scoretabs)
        self.frame.add(self.statuspane,BorderLayout.PAGE_START)
        self.frame.setVisible(True)
        self.frame.repaint()
    def setColor(self,image, id):
        '''
        Set the color after an action, such as a player movement (or selection...?)
        '''
        if id==1:
            self.score1panel.setBackground(image)
        elif id==2:
            self.score2panel.setBackground(image)
        elif id==3:
            self.score3panel.setBackground(image)
        elif id==4:
            self.score4panel.setBackground(image)

    def setScore(self,scorestring):
        self.frame.remove(self.score)
        self.score=JLabel(scorestring)
        self.frame.repaint()
    def refresh(self,score1,score2,score3,score4,tremaining):
        self.score1panel.removeAll()
        self.score2panel.removeAll()
        self.score3panel.removeAll()
        self.score4panel.removeAll()
        self.refreshtime(tremaining)
        self.score1label=JLabel("Player 1's Score:%d"%(score1))
        self.score2label=JLabel("Player 2's Score:%d"%(score2))
        self.score3label=JLabel("Player 3's Score:%d"%(score3))
        self.score4label=JLabel("Player 4's Score:%d"%(score4))
        self.score1panel.add(self.score1label)
        self.score2panel.add(self.score2label)
        self.score3panel.add(self.score3label)
        self.score4panel.add(self.score4label)
        self.score1panel.repaint()
        self.score2panel.repaint()
        self.score3panel.repaint()
        self.score4panel.repaint()

        self.frame.validate()
        self.frame.repaint()
    def refreshtime(self,tremaining):
        self.statuspane.removeAll()
        self.statusmsg=JLabel("Time remaining:%d"%(tremaining))
        self.statuspane.add(self.statusmsg)
        self.frame.validate()
        self.frame.repaint()
    def gameOver(self):
        self.statuspane.removeAll()
        self.statusmsg=JLabel("GAME OVER!!!")
        self.statuspane.add(self.statusmsg)
        self.frame.validate()
        self.frame.repaint()
    def cleanUp(self):
        self.frame.dispose()