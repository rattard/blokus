'''
Created on Feb 8, 2013

@author: Ryan Attard
blokus.ryanattard.info
CS487
"Pipe Softworks"
http://www.jython.org/jythonbook/en/1.0/LangSyntax.html
http://www.jython.org/jythonbook/en/1.0/GUIApplications.html
http://www.jython.org/jythonbook/en/1.0/TestingIntegration.html#python-testing-tools 
http://www.jython.org/jythonbook/en/1.0/JythonIDE.html

Ryan is currently developing using Eclipse, Pydev, and Jython 2.7B1. 
2.7B1 inclues necessary bug fixes.

Documentation generated with Epydoc.eg.,
    ##generate new documentation with your new comments
    attard:$HOME epydoc game.py
    ##open the base documentation page
    attard:$HOME open html/indexhtml

'''
import string
import math
import time
PLAYERLIST=["1","2","3","4"]
PLAYERCOUNT=4
MAXPIECELENGTH=5
SCREENW=1024
SCREENH=768
MAPSIZE=20
BUTTONSIZE=SCREENH/MAPSIZE
ICONSIZE="128"
ICONSET="images/icons/1_Desktop_Icons/%s%s.png"

#Game Map is a dictionary taking a string of "X,Y" as the key and the player ID as a 
#Player Pieces are a list of mini-gamemaps 
DEFAULTPIECECOORDSLIST=[["0,0"],["0,0","0,1"],["0,0","0,1","0,2"],["0,0","0,1","1,1"],["0,0","0,1","0,2","0,3"],
                         ["1,0","1,1","1,2","0,2"]]
DEFAULTPIECECOUNT=DEFAULTPIECECOORDSLIST.__len__()

#PERSONID holds an array indexed by the "CURRENTPLAYER" variable, that returns the ID of the actual person playing
#0 is computer?
#PERSONID[1;2] are players 1 and 2 (always filled)
#PERSONID[3,4] are either actual players or refer to Players 1 and 2. By default they're their own players
PERSONID=[0,1,2,3,4]
GUI=True
COLORS=["black","blue","yellow","red","green"]

class TurnState():
    '''
    Maintain who is currently up to play a piece, and what turn the game is on.
    '''
    turnCount=1
    currentPlayer=1
    numPasses=0
    firstPass=0
    def __init__(self):
        pass
    def incrementTurnCount(self):
        '''
        Increase the turn count by 1.
        '''
        self.turnCount=self.turnCount+1
    def incrementCurrentPlayer(self):
        '''
        Increment the current player. If the current player is the last for a round, then increment the turncount and return to player 1. 
        Passsing is also kept track of here, in case of final round where all four pass consecutively.
        '''
        self.numPasses=0
        self.firstPass=0
        if(self.currentPlayer<4):
            self.currentPlayer=self.currentPlayer+1
            return True
        elif(self.currentPlayer==4):
            self.currentPlayer=1
            self.incrementTurnCount()
            return True
    def passTurn(self):
        '''
        Pass current player's turn
        '''
        if(self.firstPass==0):
            self.firstPass=self.currentPlayer
        self.numPasses=self.numPasses+1
        self.currentPlayer=self.currentPlayer+1
        if(self.numPasses==4):
            self.endGame()
    def endGame(self):
        pass
class GameMapState():
    '''
    Maintain game state in a dict for the gamemap
    '''
    gamemap={}
    mapsize=MAPSIZE
    def __init__(self):
        for i in range (0,self.mapsize):
            for j in range(0,self.mapsize):
                coords=coordsToString(j,i)
                self.gamemap[coords]=0
    def printGameMapStdout(self):
        '''
        Print out the game map via stdout
        '''
        return RemainingPiecesState().printPieceStdout(self.gamemap,MAPSIZE)

    def playPiece(self,remainingpieces,xcoord,ycoord,piece,playerID,turnstate):
        '''
        Play a piece according to game rules, and modify remaining pieces and turnstate accordingly if the play is valid.
        '''
        def addPieceToBoard(xcoord,ycoord,piece):
            '''
            Add a piece directly: no verification, only called inside an if statment with clause isValidPlay(...)
            '''
            for i in range(MAXPIECELENGTH):
                for j in range(MAXPIECELENGTH):
                    if piece[coordsToString(j,i)]>0:
                        self.gamemap[coordsToString(xcoord+j,ycoord+i)]=piece[coordsToString(j,i)]
                                    
        if self.isValidPlay(xcoord, ycoord, piece,playerID,turnstate.turnCount):
            print "valid play %i"%(playerID)
            addPieceToBoard(xcoord,ycoord,piece)
            remainingpieces.removePiece(playerID,piece)
            turnstate.incrementCurrentPlayer()
            return True
        print "invalid play %i"%(playerID)
        return False
    
    def isValidPlay(self,xbase,ybase,piece,currentplayer,turncount):
        '''
        verify the piece is not violating direct game rule
        '''
        if self.isOverlap(xbase,ybase,piece):
            print "is overlap edge of board or other piece"
            return False
        if self.isTouchingOwnEdge(xbase,ybase,currentplayer):
            print "touching own edge"
            return False
        if not self.isTouchingOwnCorner(xbase,ybase,piece,currentplayer,turncount):
            print "not touching own corner, player %i"%(currentplayer)
            return False
        return True

    def isOverlap(self,xbase,ybase,piece):
        '''
        verify the piece is not overlapping a currently occupied tile
        '''
        for i in range(MAXPIECELENGTH):
            for j in range(MAXPIECELENGTH):
                #if the piece is actually using that part of the gamemap
                if piece[coordsToString(j,i)]>0:
                    xcoord=j+xbase
                    ycoord=i+ybase
                    #direct violation by overlap with game boundary
                    if (xcoord>=MAPSIZE or ycoord>=MAPSIZE):
                        print "out of bounds %i,%i"%(xcoord,ycoord)
                        return True
                    if self.gamemap[coordsToString((xcoord),(ycoord))]>0:
                        print "overlap existing tile"
                        return True
        return False

    def isTouchingOwnEdge(self,xcoord,ycoord,playerID):
        '''
        Verify piece being played doesn't touch the existing edge
        '''
        if(xcoord-1>0 and self.gamemap[coordsToString((xcoord-1),(ycoord))]==playerID):
            return True
        if(xcoord+1<MAPSIZE and self.gamemap[coordsToString((xcoord+1),(ycoord))]==playerID):
            return True
        if(ycoord-1>0 and self.gamemap[coordsToString((xcoord),(ycoord-1))]==playerID):
            return True
        if(ycoord+1<MAPSIZE and self.gamemap[coordsToString((xcoord),(ycoord+1))]==playerID):
            return True
        return False

    def isTouchingOwnCorner(self,xbase,ybase,piece,playerID,turncount):
        '''
        Verify piece being played touches a corner of the previously played pieces
        '''
        for i in range(MAXPIECELENGTH):
            for j in range(MAXPIECELENGTH):
                #if the piece is actually using that part of the gamemap
                if piece[coordsToString(j,i)]>0:
                    xcoord=j+xbase
                    ycoord=i+ybase
                    if(turncount==1):
                        if self.isCorner:
                            return True
                    if(xcoord-1>=0 and ycoord-1>=0 and 
                       self.gamemap[coordsToString((xcoord-1),(ycoord-1))]==playerID):
                        return True
                    if(xcoord+1<MAPSIZE and ycoord-1>=0 and 
                       self.gamemap[coordsToString((xcoord+1),(ycoord-1))]==playerID):
                        return True
                    if(xcoord-1>=0and ycoord+1<MAPSIZE and 
                       self.gamemap[coordsToString((xcoord-1),(ycoord+1))]==playerID):
                        return True
                    if(xcoord+1<MAPSIZE and ycoord+1<MAPSIZE and 
                       self.gamemap[coordsToString((xcoord+1),(ycoord+1))]==playerID):
                        return True

        return False
    def isCorner(self,xcoord,ycoord):
        '''
        Return true if the coordinate is a corner of the game map
        '''
        if(xcoord==0 and ycoord==0):
            return True
        if(xcoord==0 and ycoord==(MAPSIZE-1)):
            return True
        if(xcoord==(MAPSIZE-1) and ycoord==0):
            return True
        if(xcoord==(MAPSIZE-1) and ycoord==(MAPSIZE-1)):
            return True 
        return False
class RemainingPiecesState():
    '''
    Maintain Player Pieces states together in a list of lists
    '''
    color1pieces=[]
    color2pieces=[]
    color3pieces=[]
    color4pieces=[]
    playerpiecelist=[color1pieces,color2pieces,color3pieces,color4pieces]
    def __init__(self):
        for i in range(1,5):
            self.addDefaultPieces(i)
 

    def addDefaultPieces(self,playerid):
        '''
        Add the default pieces for the given player ID (1-4)
        Default Pieces taken from http://www.dbz.com.au/blokus_pieces.htm
        '''
        coordslist=DEFAULTPIECECOORDSLIST
        for pieceCoords in coordslist:
            self.playerpiecelist[playerid-1].append(self.pieceFromCoordsList(pieceCoords,playerid))
        return
    def removePiece(self,playerid,piece):
        self.playerpiecelist[playerid-1].remove(piece)

    def pieceFromCoordsList(self,coordList,playerid):
        '''
        Take a list of coordinates of blocks and make a return a piece following those coords
        '''
        piece={}
        for i in range(coordList.__len__()):
            piece[coordList[i]]=playerid
        piece=self.fillEmptySpaces(piece)
        return piece
#


    def fillEmptySpaces(self,piece):
        '''
        Fill the dict with empty spaces after the pieceFromCoordsList function 
        '''
        for i in range(MAXPIECELENGTH):
            for j in range(MAXPIECELENGTH):
                #Note the j,i not i,j. This is because you go row-wise
                if not piece.has_key(coordsToString(j,i)):
                    #Note the j,i not i,j. This is because you go row-wise
                    piece[coordsToString(j,i)]=0
        return piece    

    def printPieceStdout(self,piece,piecelength=None):
        '''
        Print a piece with a nice formatting on the cmdline
        '''
        if piecelength==None:
            piecelength=MAXPIECELENGTH
        labelw=int(math.ceil(math.log10(piecelength))+2)
        #Title Line with Number Indicies
        line="".rjust(labelw)
        for i in range(piecelength):
            line=line+(("%i " % (i)).rjust(labelw-1))
        print line
        #Offset for 2 digit numbers
        line="".rjust(labelw)
        for i in range(piecelength):
            line=("%i "% (i)).rjust(labelw)
            for j in range(piecelength):
                #Note the j,i not i,j. This is because you go row-wise
                line=line+(("%s "% (piece[coordsToString(j,i)])).rjust(labelw-1))
            print line
        endline=""
        for i in range(line.__len__()):
            endline=endline+"-"
        print endline
        return
    #List Remaining Pieces
    def listPlayerPiecesStdout(self,pieces):
            for i in range(pieces.__len__()):
                print "Piece #%i"%(i)
                self.printPieceStdout(pieces[i])

from javax.swing import JFrame,JButton,WindowConstants,ImageIcon,AbstractAction,BorderFactory
from java.awt import GridLayout,Color
from java.net import URL

class GameMapGUI:
    '''
    A JFrame that contains the GameMapGUI.
    '''
    gametiles={}
    def __init__(self):
        frame = JFrame("Blokus")
        frame.setLayout(GridLayout(MAPSIZE,MAPSIZE))
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
        frame.setSize(SCREENW, SCREENH)
        for i in range(MAPSIZE):
            for j in range(MAPSIZE):
                t=TileGUI(j,i,"black")
                frame.add(t)
                self.gametiles["%i,%i"%(j,i)]=t
        #frame.pack()
        frame.setVisible(True)
    def refresh(self,gameMapState):
        '''
        Refresh the GameMapGUI after a change in the GameMapState, such as after a piece being played.
        '''
        for j in range (gameMapState.mapsize):
            for i in range(gameMapState.mapsize):
                self.gametiles[coordsToString(j, i)].setColor(COLORS[gameMapState.gamemap[coordsToString(j,i)]])
class TileGUI(JButton):
    '''
    An individual tile on the gameboard. Either empty (black), or occupied by a player's color
    '''
    def __init__(self,x,y,color):
        iconImage=ImageIcon(URL("file","",ICONSET%(color,ICONSIZE)))
        self.setIcon(iconImage)
        self.setLocation(x, y)
        self.setSize(BUTTONSIZE, BUTTONSIZE)
        self.setToolTipText(coordsToString(x,y))
        self.setBorder(BorderFactory.createLineBorder(Color.white))
        self.setAction(TileActionGUI(coordsToString(x,y)))
    def setColor(self,color):
        '''
        Set the color after an action, such as a player movement (or selection...?)
        '''
        iconImage=ImageIcon(URL("file","",ICONSET%(color,ICONSIZE)))
        self.setIcon(iconImage)
class TileActionGUI(AbstractAction):
    '''
    On click action for the tiles! hooray, watch the stdout for the coordinates of your clicks
    '''
    location="-1,-1"
    def __init__(self,givenlocation):
        self.location=givenlocation
        pass
    def actionPerformed(self,e):
        print self.location
def coordsToString(x,y):
    '''
    Return a string 'x,y' to be used in game map and piece dicts from two ints
    
    "%i,%i"%(x,y)
    '''
    string="%i,%i" % (x,y)
    return (string)
def stringToCoords(coords):
    '''
    Return a pair of ints x,y from a string label
    
    There's no whitespace, just %i,%i
    '''
    coordlist=string.split(coords, ',')
    return (coordlist[0],coordlist[1])


def getPlayerInformationStdin():
    '''
    Get player information stdin. Mostly structures how player data could be saved later
    '''
    playerCountEntered=string.atoi(raw_input("Enter number of players(2-4):").strip())
    while playerCountEntered > 4 or playerCountEntered < 2:
        playerCountEntered=string.atoi(raw_input("Enter number of players(2-4):").strip())
    globals()['PLAYERCOUNT']=playerCountEntered
    #clear default playerlist or restore settings for individual games
    while PLAYERLIST.__len__()>0:PLAYERLIST.pop();
    for i in range (1 ,playerCountEntered+1):
        #Steal away the newline
        PLAYERLIST.append(raw_input("Enter the your name player %i:"%(i)))
    if PLAYERCOUNT==2:
        PERSONID[3]=1
        PERSONID[4]=2
        print "%s will play for the 3rd person, %s will play last"%(PLAYERLIST[0],PLAYERLIST[1])
    elif PLAYERCOUNT==3:
        PERSONID[3]=1
        print "%s gets the extra plays by default"%(PLAYERLIST[1])



def playGame():
    '''
    Demo area for the game. Currently, plays first 4 pieces, then fails a piece and waits 60 seconds 
    (before quitting)
    '''
    gameMapState=GameMapState()
    gamePieces=RemainingPiecesState()
    turn=TurnState()


    if not GUI:
        getPlayerInformationStdin()
        gameMapState.printGameMapStdout()
        
    else:
        g=GameMapGUI()
        g.refresh(gameMapState)
        gameMapState.playPiece(gamePieces,19,16,gamePieces.playerpiecelist[turn.currentPlayer-1][4],
                               turn.currentPlayer,turn)
        time.sleep(1)
        g.refresh(gameMapState)
        gameMapState.playPiece(gamePieces,19,0,gamePieces.playerpiecelist[turn.currentPlayer-1][4],
                               turn.currentPlayer,turn)
        time.sleep(1)
        g.refresh(gameMapState)
        gameMapState.playPiece(gamePieces,0,16,gamePieces.playerpiecelist[turn.currentPlayer-1][4],
                               turn.currentPlayer,turn)
        time.sleep(1)
        g.refresh(gameMapState)
        gameMapState.playPiece(gamePieces,0,0,gamePieces.playerpiecelist[turn.currentPlayer-1][4],
                               turn.currentPlayer,turn)
        time.sleep(1)
        g.refresh(gameMapState)
        gameMapState.playPiece(gamePieces,0,0,gamePieces.playerpiecelist[turn.currentPlayer-1][4],
                               turn.currentPlayer,turn)
        time.sleep(1)
        g.refresh(gameMapState)
        time.sleep(60)
    return

if __name__ == '__main__':
        playGame()
        exit()
